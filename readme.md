# Roads administration -- bridges data

ref. ticket https://gitlab.com/geoportalksk/projectmanagement/-/issues/27

data source : https://www.cdb.sk/files/documents/cestna-databanka/vystupy-cdb/2022/csv/sr_co_mosty-dc_zoznam_2022-01-01.csv

## Working notes
We try to use VRT + maybe a bit of scripting to do the data integration

### Step 1: get the data in UTF8
I didn't find, as of now, a way to use a csv that is not in UTF-8 => we need to download the file, then convert it to utf8:

```bash
wget https://www.cdb.sk/files/documents/cestna-databanka/vystupy-cdb/2022/csv/sr_co_mosty-dc_zoznam_2022-01-01.csv
uconv -f windows-1250 -o sr_co_mosty-dc_zoznam_2022-01-01-utf8.csv sr_co_mosty-dc_zoznam_2022-01-01.csv
rm sr_co_mosty-dc_zoznam_2022-01-01.csv
```

### Step 2: create the VRT file
Open it with QGIS (Spreadsheet Layer extension) -> it generates a vrt file
Renamed it to easier name and edited the content: look for roads_administration__bridges.vrt

### Step 3: Push to DB
I created an `ssc` schema, for the Slovak Raods Administration.

First, create an SSH tunnel to connect to the DB

Then:
```bash
ogr2ogr -lco DROP_TABLE=ON -lco CREATE_SCHEMA=OFF -f PGDUMP -lco SCHEMA=ssc -lco FID=gid -lco SPATIAL_INDEX=GIST --config PG_USE_COPY YES /vsistdout/ roads_administration__bridges.vrt \
	| psql -h localhost -p 15432 -d georchestra -U jpommier
```

**Note** : to publish only one of the tables in the vrt "geopackage", you can add the following option in the ogr2ogr command: `-sql "SELECT * FROM bridges_dc" `


### Step 4: join the 2 datasets back into single entries: we merge the non-geographical data (Most/bridges) in the geographical ones (DC/dilatation sections):
We can configure the view either from GeoServer, or directly on the DB (will be visible as any other tables).

- If we configure the view in GeoServer (new Layer -> from SQL view)
```sql
CREATE VIEW ssc.bridges_dc_full AS (
  SELECT dc.*, b.bridge_name, b.bridge_id, b.management_number, b.year_of_construction, b.stn_load_class, b.nb_of_holes, b.bridge_length_meters, b.supporting_structure_length_meters, b.clear_width_meters, b.width_between_bezels_meters, b.bridge_structure_area_m2, b.bridge_area_m2
  FROM ssc.bridges_dc AS dc LEFT JOIN ssc.bridges AS b ON starts_with(dc.dc_id, b.bridge_id)
```
- If we want to create it on the DB directly:
```bash
psql -h localhost -p 15432 -d georchestra -U jpommier -c "CREATE VIEW ssc.bridges_dc_full AS (
  SELECT dc.*, b.bridge_name, b.bridge_id, b.management_number, b.year_of_construction, b.stn_load_class, b.nb_of_holes, b.bridge_length_meters, b.supporting_structure_length_meters, b.clear_width_meters, b.width_between_bezels_meters, b.bridge_structure_area_m2, b.bridge_area_m2
  FROM ssc.bridges_dc AS dc LEFT JOIN ssc.bridges AS b ON starts_with(dc.dc_id, b.bridge_id)
);"
```
