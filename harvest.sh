#!/bin/bash

# Get the data
wget https://www.cdb.sk/files/documents/cestna-databanka/vystupy-cdb/2022/csv/sr_co_mosty-dc_zoznam_2022-01-01.csv
# Change encoding to UTF8
uconv -f windows-1250 -o sr_co_mosty-dc_zoznam_utf8.csv sr_co_mosty-dc_zoznam_2022-01-01.csv

# Publish the content: there will be as many tables as OGRLayer listed in the VRT
ogr2ogr -lco DROP_TABLE=ON -lco CREATE_SCHEMA=OFF -f PGDUMP -lco SCHEMA=ssc -lco FID=gid -lco SPATIAL_INDEX=GIST --config PG_USE_COPY YES /vsistdout/ roads_administration__bridges.vrt \
	| psql -h localhost -p 15432 -d georchestra -U jpommier

# clean up
rm sr_co_mosty-dc_zoznam_2022-01-01.csv
rm sr_co_mosty-dc_zoznam_utf8.csv
